package com.example.android.github.pullrequests;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.RequestManager;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.example.android.github.R;
import com.example.android.github.pojos.PR;

import java.util.ArrayList;



public class PRRViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<PR> prs;
    private RequestManager glide;
    private pullRequestsContract.presenter presenter;
    private Context context;

    private static final int CARREGANDO = 111;
    private static final int FIM = 222;


    public PRRViewAdapter(Context context, ArrayList<PR> prs, RequestManager glide, pullRequestsContract.presenter presenter) {
        this.context = context;
        this.prs = prs;
        this.glide = glide;
        this.presenter = presenter;
    }


    @Override
    public int getItemViewType(int position) {
        if ((prs.get(position).isCarregando())) {
            return CARREGANDO;
        } else if ((prs.get(position).isFim())) {
            return FIM;
        } else {
            return super.getItemViewType(position);
        }
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view;
        switch (viewType) {
            case CARREGANDO:
                view = inflater.inflate(R.layout.item_carregando, parent, false);
                return new PRCarregandoViewHolder(view);

            case FIM:
                view = inflater.inflate(R.layout.item_fim, parent, false);
                return new PRFimViewHolder(view);

            default:
                view = inflater.inflate(R.layout.item_pr, parent, false);
                return new PRViewHolder(view);
        }
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof PRCarregandoViewHolder) {
            PRCarregandoViewHolder vh = (PRCarregandoViewHolder) holder;
        } else if (holder instanceof PRFimViewHolder) {
            PRFimViewHolder PR = (PRFimViewHolder) holder;
        } else {
            PRViewHolder vh = (PRViewHolder) holder;
            vh.bind(context, prs, position, glide, presenter);
        }
    }


    @Override
    public int getItemCount() {
        return prs.size();
    }
}
