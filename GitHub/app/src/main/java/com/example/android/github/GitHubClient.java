package com.example.android.github;

import com.example.android.github.pojos.MainPojoRepos;
import com.example.android.github.pojos.PR;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface GitHubClient {

    @GET("/search/repositories")
    Call<MainPojoRepos> javaRepos(
            @Query(value = "q", encoded = true) String language,
            @Query("sort") String sort,
            @Query("page") Integer page
    );

    @GET("/repos/{criador}/{repositorio}/pulls?state=all")
    Call<ArrayList<PR>> pullRequest(
            @Path("criador") String criador,
            @Path("repositorio") String repositorio,
            @Query("page") Integer page
    );

}