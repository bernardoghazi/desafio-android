package com.example.android.github.repositories;


import com.example.android.github.GitHubClient;
import com.example.android.github.pojos.MainPojoRepos;

public interface repositoriesContract {
    interface view {
        void receberRepos(MainPojoRepos mainPojo);
    }

    interface presenter {
        void getRepos(GitHubClient client, int pag);
        void startPRActivity(String criador, String repositorio);
        void receberRepos (MainPojoRepos mainPojo);
    }

    interface model {
        void loadRepos(GitHubClient client, int pag);
    }
}
