package com.example.android.github.repositories;

import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.RequestManager;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.example.android.github.R;
import com.example.android.github.pojos.Repo;

import java.util.ArrayList;



public class RepoViewHolder extends RecyclerView.ViewHolder {

    public ConstraintLayout layout;
    public TextView nome_repo, descricao, username, nomeSobrenome, starsNumber, forksNumber;
    public ImageView avatar;
    public View linha_bottom;


    public RepoViewHolder(View itemView) {
        super(itemView);
        layout = itemView.findViewById(R.id.repo_layout);
        nome_repo = itemView.findViewById(R.id.nome_repo);
        descricao = itemView.findViewById(R.id.descricao_repo);
        starsNumber = itemView.findViewById(R.id.stars_number);
        forksNumber = itemView.findViewById(R.id.forks_number);
        username = itemView.findViewById(R.id.username_repo);
        avatar = itemView.findViewById(R.id.avatar_repo);
        linha_bottom = itemView.findViewById(R.id.linha_bottom_repo);
    }


    void bind(ArrayList<Repo> repos, int position, RequestManager glide, final repositoriesContract.presenter presenter) {

        final Repo repo = repos.get(position);

        descricao.setText(repo.getDescription());
        username.setText(repo.getOwner().getLogin());
        starsNumber.setText(Integer.toString(repo.getStargazers_count()));
        nome_repo.setText(repo.getName());
        forksNumber.setText(Integer.toString(repo.getForks_count()));

        Drawable placeholder_drawable = ContextCompat.getDrawable(itemView.getContext(), R.drawable.ic_carregando);
        ((Animatable)placeholder_drawable).stop();
        ((Animatable)placeholder_drawable).start();

        glide.load(repo.getOwner().getAvatar_url())
                .placeholder(placeholder_drawable)
                .error(R.drawable.ic_erro)
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        Log.d("meuGLIDE", "Exception:" + e.toString());
                        return false;
                    }


                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
//                            Log.d("meuGLIDE", "ResourceReady.");
                        return false;
                    }
                })
                .into(avatar);


        //linha divisória não deve aparecer após o último item.
        if (position == (repos.size() - 1)) {
            linha_bottom.setVisibility(View.INVISIBLE);
        } else {
            linha_bottom.setVisibility(View.VISIBLE);
        }

        layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.startPRActivity(repo.getOwner().getLogin(), repo.getName());
            }
        });
    }
}
