package com.example.android.github.repositories;

import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.android.github.EndlessScrollListener;
import com.example.android.github.GitHubClient;
import com.example.android.github.R;
import com.example.android.github.VerificaConexao;
import com.example.android.github.pojos.MainPojoRepos;
import com.example.android.github.pojos.Repo;

import java.util.ArrayList;

import okhttp3.Cache;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RepoActivity extends AppCompatActivity implements repositoriesContract.view {

    repositoriesContract.presenter presenter;
    ImageView carregando;
    ConstraintLayout layoutPrincipal;
    RecyclerView rView;
    ArrayList<Repo> repos = new ArrayList<>();
    RepoRViewAdapter adapter;
    TextView semConexao;
    LinearLayoutManager linearLayoutManager;
    Toolbar toolbar;

    private static String STATE = "state";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_repo);

        carregando = findViewById(R.id.carregando_repo_activity);
        layoutPrincipal = findViewById(R.id.layout_principal_repo);
        presenter = new RepoPresenter(this, this);
        rView = findViewById(R.id.rViewRepo);
        semConexao = findViewById(R.id.SemConexaoRepoTV);
        linearLayoutManager = new LinearLayoutManager(this);
        rView.setLayoutManager(linearLayoutManager);
        toolbar = findViewById(R.id.repo_toolbar);
        setSupportActionBar(toolbar);

        if (savedInstanceState != null) {
            repos = savedInstanceState.getParcelableArrayList(STATE);
        }

        adapter = new RepoRViewAdapter(this, repos, Glide.with(this), presenter);
        rView.setAdapter(adapter);
    }


    @Override
    protected void onResume() {
        super.onResume();

        Drawable carregando_anim = carregando.getDrawable();
        ((Animatable) carregando_anim).stop();
        ((Animatable) carregando_anim).start();

        carregando.setVisibility(View.VISIBLE);
        layoutPrincipal.setVisibility(View.GONE);

        int cacheSize = 10 * 1024 * 1024;
        Cache cache = new Cache(getCacheDir(), cacheSize);

        OkHttpClient httpClient = new OkHttpClient.Builder()
                .cache(cache)
                .build();

        String API_BASE_URL = "https://api.github.com";

        Retrofit.Builder builder =
                new Retrofit.Builder()
                        .baseUrl(API_BASE_URL)
                        .addConverterFactory(GsonConverterFactory.create())
                        .client(httpClient);

        Retrofit retrofit = builder.build();

        final GitHubClient client = retrofit.create(GitHubClient.class);





        EndlessScrollListener scrollListener = new EndlessScrollListener(linearLayoutManager) {
            @Override
            public void loadPage(int page, RecyclerView view) {
                presenter.getRepos(client, page);
            }
        };



        rView.addOnScrollListener(scrollListener);




        VerificaConexao vc = new VerificaConexao(this);
        if (vc.isConnected()) {
            Log.d("CONEXÃO", "ok");
            semConexao.setVisibility(View.GONE);
            rView.setVisibility(View.VISIBLE);
            presenter.getRepos(client, 1);
        } else {
            Log.d("CONEXÃO", "falhou");
            semConexao.setVisibility(View.VISIBLE);
            rView.setVisibility(View.GONE);
        }
    }





    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putParcelableArrayList(STATE, repos);
        super.onSaveInstanceState(outState);
    }





    @Override
    public void receberRepos(MainPojoRepos mainPojo) {


        if (mainPojo == null || mainPojo.getItems().size() == 0) {//se a lista retornada for null (limite da API) ou vazia (chegou ao fim da lista)

            if (repos.size() != 0) {
                repos.remove(repos.size() - 1);

                Repo fim = new Repo();
                fim.setFim(true);

                repos.add(fim);

                rView.post(new Runnable() {
                    @Override
                    public void run() {
                        adapter.notifyItemRangeChanged(repos.size() + 1, 1);//Apenas o último elemento mudou.
                    }
                });
            }


        } else { //se a lista retornada TIVER conteúdo

            ArrayList<Repo> novosRepos = mainPojo.getItems();

            if (repos.size() == 0) {
                repos.addAll(novosRepos);

                Repo carregando = new Repo();
                carregando.setCarregando(true);

                repos.add(carregando);


                rView.post(new Runnable() {
                    @Override
                    public void run() {
                        adapter.notifyDataSetChanged();
                    }
                });
            } else {
                repos.remove(repos.size() - 1);

                final int totalDeItensAnterior = repos.size();

                repos.addAll(novosRepos);

                Repo carregando = new Repo();
                carregando.setCarregando(true);
                repos.add(carregando);


                rView.post(new Runnable() {
                    @Override
                    public void run() {
                        Log.d("TAMANHO", Integer.toString(repos.size()));
                        adapter.notifyItemRangeChanged(totalDeItensAnterior, 30);
                    }
                });
            }
        }
        carregando.setVisibility(View.GONE);
        layoutPrincipal.setVisibility(View.VISIBLE);
    }
}