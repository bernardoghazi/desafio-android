package com.example.android.github.repositories;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.RequestManager;
import com.example.android.github.R;
import com.example.android.github.pojos.Repo;

import java.util.ArrayList;



public class RepoRViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<Repo> repos;
    private RequestManager glide;
    private Context context;
    private repositoriesContract.presenter presenter;

    private static final int CARREGANDO = 111;
    private static final int FIM = 222;


    public RepoRViewAdapter(Context context, ArrayList<Repo> repos, RequestManager glide, repositoriesContract.presenter presenter) {
        this.context = context;
        this.repos = repos;
        this.glide = glide;
        this.presenter = presenter;
    }


    @Override
    public int getItemViewType(int position) {
        if ((repos.get(position).isCarregando())) {
            return CARREGANDO;
        } else if ((repos.get(position).isFim())) {
            return FIM;
        } else {
            return super.getItemViewType(position);
        }
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view;
        switch (viewType) {
            case CARREGANDO:
                view = inflater.inflate(R.layout.item_carregando, parent, false);
                return new RepoCarregandoViewHolder(view);

            case FIM:
                view = inflater.inflate(R.layout.item_fim, parent, false);
                return new RepoFimViewHolder(view);

            default:
                view = inflater.inflate(R.layout.item_repo, parent, false);
                return new RepoViewHolder(view);
        }

    }



    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if(holder instanceof RepoCarregandoViewHolder) {
            RepoCarregandoViewHolder vh = (RepoCarregandoViewHolder) holder;
        } else if (holder instanceof RepoFimViewHolder) {
            RepoFimViewHolder vh = (RepoFimViewHolder) holder;
        } else {
            RepoViewHolder vh = (RepoViewHolder) holder;
            vh.bind(repos, position, glide, presenter);
        }

    }


    @Override
    public int getItemCount() {
        return repos.size();
    }
}
