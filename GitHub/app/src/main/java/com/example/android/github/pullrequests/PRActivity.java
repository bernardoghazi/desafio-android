package com.example.android.github.pullrequests;


import android.content.Intent;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.android.github.EndlessScrollListener;
import com.example.android.github.GitHubClient;
import com.example.android.github.R;
import com.example.android.github.VerificaConexao;
import com.example.android.github.pojos.PR;

import java.util.ArrayList;

import okhttp3.Cache;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class PRActivity extends AppCompatActivity implements pullRequestsContract.view {

    pullRequestsContract.presenter presenter;
    ImageView carregando;
    ConstraintLayout layoutPrincipal;
    RecyclerView rView;
    TextView rVVazia;
    ArrayList<PR> PRs = new ArrayList<>();
    PRRViewAdapter adapter;
    TextView semConexao;
    LinearLayoutManager linearLayoutManager;
    Toolbar toolbar;
    private String repositorio;
    TextView pr_nome;
    ImageView state;

    private static String STATE = "state";



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pr);

        carregando = findViewById(R.id.carregando_pr_activity);
        layoutPrincipal = findViewById(R.id.layout_principal_pr);
        presenter = new PRPresenter(this, this);
        rView = findViewById(R.id.rViewPR);
        rVVazia = findViewById(R.id.rv_vazia_pr);
        semConexao = findViewById(R.id.SemConexaoPRTV);
        pr_nome = findViewById(R.id.PR_nome);
        linearLayoutManager = new LinearLayoutManager(this);
        rView.setLayoutManager(linearLayoutManager);
        toolbar = findViewById(R.id.pr_toolbar);
        state = findViewById(R.id.state_iv);
        toolbar.setNavigationIcon(R.drawable.ic_ab_back_material);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (savedInstanceState != null) {
            PRs = savedInstanceState.getParcelableArrayList(STATE);
        }

        adapter = new PRRViewAdapter(this, PRs, Glide.with(this), presenter);
        rView.setAdapter(adapter);
    }



    @Override
    protected void onResume() {
        super.onResume();

        Drawable carregando_anim = carregando.getDrawable();
        ((Animatable) carregando_anim).stop();
        ((Animatable) carregando_anim).start();

        carregando.setVisibility(View.VISIBLE);
        layoutPrincipal.setVisibility(View.GONE);

        int cacheSize = 10 * 1024 * 1024;
        Cache cache = new Cache(getCacheDir(), cacheSize);

        OkHttpClient httpClient = new OkHttpClient.Builder()
                .cache(cache)
                .build();

        String API_BASE_URL = "https://api.github.com";

        Retrofit.Builder builder =
                new Retrofit.Builder()
                        .baseUrl(API_BASE_URL)
                        .addConverterFactory(GsonConverterFactory.create())
                        .client(httpClient);

        Retrofit retrofit = builder.build();

        final GitHubClient client = retrofit.create(GitHubClient.class);


        Intent intent = getIntent();
        final String criador = intent.getStringExtra("criador");
        repositorio = intent.getStringExtra("repositorio");

        pr_nome.setText(repositorio);


        EndlessScrollListener scrollListener = new EndlessScrollListener(linearLayoutManager) {
            @Override
            public void loadPage(int page, RecyclerView view) {
                presenter.getPRs(client, criador, repositorio, page);
            }
        };

        rView.addOnScrollListener(scrollListener);





        VerificaConexao vc = new VerificaConexao(this);
        if (vc.isConnected()) {
            Log.d("CONEXÃO", "ok");
            semConexao.setVisibility(View.GONE);
            rView.setVisibility(View.VISIBLE);
            presenter.getPRs(client, criador, repositorio, 1);
        } else {
            Log.d("CONEXÃO", "falhou");
            semConexao.setVisibility(View.VISIBLE);
            rView.setVisibility(View.GONE);
        }
    }





    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putParcelableArrayList(STATE, PRs);
        super.onSaveInstanceState(outState);
    }





    //Sem isso a RepoActivity voltava pro 1o item ao invés de manter o estado.
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }





    @Override
    public void receberPRs(ArrayList<PR> ListPR) {


        rView.setVisibility(View.VISIBLE);
        rVVazia.setVisibility(View.GONE);

        if (ListPR == null || ListPR.size() == 0) {//se a lista retornada for null (limite da API) ou vazia (chegou ao fim da lista)

            if (PRs.size() != 0) {
                PRs.remove(PRs.size() - 1);

                PR fim = new PR();
                fim.setFim(true);

                PRs.add(fim);


                rView.post(new Runnable() {
                    @Override
                    public void run() {
                        adapter.notifyItemRangeChanged(PRs.size() + 1, 1);//Apenas o último elemento mudou.
                    }
                });


            } else {
                rView.setVisibility(View.GONE);
                rVVazia.setVisibility(View.VISIBLE);
            }


        } else { //se a lista retornada TIVER conteúdo


            if (PRs.size() == 0) {
                PRs.addAll(ListPR);

                PR carregando = new PR();
                carregando.setCarregando(true);

                PRs.add(carregando);


                rView.post(new Runnable() {
                    @Override
                    public void run() {
                        adapter.notifyDataSetChanged();
                    }
                });

            } else {
                PRs.remove(PRs.size() - 1);

                final int totalDeItensAnterior = PRs.size();

                PRs.addAll(ListPR);

                PR carregando = new PR();
                carregando.setCarregando(true);
                PRs.add(carregando);


                rView.post(new Runnable() {
                    @Override
                    public void run() {
                        adapter.notifyItemRangeChanged(totalDeItensAnterior, 30);
                    }
                });
            }
        }
        carregando.setVisibility(View.GONE);
        layoutPrincipal.setVisibility(View.VISIBLE);
    }
}