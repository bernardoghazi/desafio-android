package com.example.android.github.repositories;

import android.support.annotation.NonNull;
import android.util.Log;

import com.example.android.github.GitHubClient;
import com.example.android.github.pojos.MainPojoRepos;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RepoModel implements repositoriesContract.model{

    private repositoriesContract.presenter presenter;


    RepoModel(repositoriesContract.presenter presenter) {
        this.presenter = presenter;
    }


    @Override
    public void loadRepos(GitHubClient client, int pag) {
        //carrega as repos
//        Log.d("GETREPOS", "página " + Integer.toString(pag));
        Call<MainPojoRepos> callRepos = client.javaRepos("language:Java", "stars", pag);
//        Log.d("RESPOSTA_JAVA_REPOS", callRepos.request().url().toString());

        callRepos.enqueue(new Callback<MainPojoRepos>() {
            @Override
            public void onResponse(@NonNull Call<MainPojoRepos> call, @NonNull Response<MainPojoRepos> response) {
                Log.d("RESPOSTA_JAVA_REPOS", "foi");
                presenter.receberRepos(response.body());

                if (response.raw().cacheResponse() != null && response.raw().networkResponse() != null) {
                    // true: response was served from cache
                    Log.d("RESPOSTA_JAVA_REPOS", "conteúdo veio do CACHE, header veio do SERVIDOR ('304 - not modified')");
                    Log.d("RESPOSTA_JAVA_REPOS", response.raw().toString());
                } else if (response.raw().cacheResponse() != null) {
                    // true: response was served from cache
                    Log.d("RESPOSTA_JAVA_REPOS", "veio do CACHE");
                    Log.d("RESPOSTA_JAVA_REPOS", response.raw().cacheResponse().toString());
                } else if (response.raw().networkResponse() != null) {
                    // true: response was served from network/server
                    Log.d("RESPOSTA_JAVA_REPOS", "veio do SERVIDOR");
                    Log.d("RESPOSTA_JAVA_REPOS", response.raw().networkResponse().toString());
                }
            }


            @Override
            public void onFailure(@NonNull Call<MainPojoRepos> call, @NonNull Throwable t) {
                Log.d("RESPOSTA", "falhou");
                if (t instanceof IOException) {
                    Log.d("RESPOSTA_JAVA_REPOS", "erro de conexão -> IOException");
                } else {
                    Log.d("RESPOSTA_JAVA_REPOS", "algum outro erro");
                }
            }
        });
    }
}
