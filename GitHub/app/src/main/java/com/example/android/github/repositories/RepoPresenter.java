package com.example.android.github.repositories;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.example.android.github.GitHubClient;
import com.example.android.github.pojos.MainPojoRepos;
import com.example.android.github.pullrequests.PRActivity;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RepoPresenter implements repositoriesContract.presenter {

    private Context context;
    private repositoriesContract.view view;
    private repositoriesContract.model model;


    RepoPresenter(Context context, repositoriesContract.view view) {
        this.context = context;
        this.view = view;
        model = new RepoModel(this);
    }


    @Override
    public void getRepos(GitHubClient client, int pag) {
        model.loadRepos(client, pag);
    }


    @Override
    public void startPRActivity(String criador, String repositorio) {
        Intent intent = new Intent(context, PRActivity.class);
        intent.putExtra("criador", criador);
        intent.putExtra("repositorio", repositorio);
        context.startActivity(intent);
    }


    @Override
    public void receberRepos(MainPojoRepos mainPojo) {
        view.receberRepos(mainPojo);
    }
}
