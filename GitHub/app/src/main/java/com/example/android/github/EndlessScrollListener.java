package com.example.android.github;


import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

public abstract class EndlessScrollListener extends RecyclerView.OnScrollListener {
    private int pagInicial = 1;
    // Mínimo de itens abaixo do último visível antes de carregar outra página
    private int reservaDeItems = 5;
    private int pagAtual = 1;
    // totald e itens após p último load
    private int totalItensAnterior = 0;
    private boolean loading = true;

    RecyclerView.LayoutManager layoutManager;


    public EndlessScrollListener(LinearLayoutManager layoutManager) {
        this.layoutManager = layoutManager;
    }


    @Override
    public void onScrolled(RecyclerView view, int dx, int dy) {
        int lastVisibleItemPosition = 0;
        int totalItens = layoutManager.getItemCount();

        lastVisibleItemPosition = ((LinearLayoutManager) layoutManager).findLastVisibleItemPosition();

        if (totalItens < totalItensAnterior) {
            this.pagAtual = this.pagInicial;
            this.totalItensAnterior = totalItens;
            if (totalItens == 0) {
                this.loading = true;
            }
        }

        if (loading && (totalItens > totalItensAnterior)) {
            loading = false;
            totalItensAnterior = totalItens;
        }

        if (!loading && (lastVisibleItemPosition + reservaDeItems) > totalItens) {//ou lastVisibleItemPosition > (totalItens-reservaDeItens)
            pagAtual++;
            loadPage(pagAtual, view);
            loading = true;
        }
    }


    public void reset() {
        this.pagAtual = this.pagInicial;
        this.totalItensAnterior = 0;
        this.loading = true;
    }


    public abstract void loadPage(int page, RecyclerView view);

}