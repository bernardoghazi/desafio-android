package com.example.android.github.pojos;


import android.os.Parcel;
import android.os.Parcelable;

public class PR implements Parcelable {
    private String title;
    private String body;
    private String html_url;
    private String created_at;
    private String state;
    private User user;
    private boolean carregando;
    private boolean fim;


    public boolean isCarregando() {
        return carregando;
    }


    public void setCarregando(boolean carregando) {
        this.carregando = carregando;
    }


    public boolean isFim() {
        return fim;
    }


    public void setFim(boolean fim) {
        this.fim = fim;
    }


    public String getTitle() {
        return title;
    }


    public String getBody() {
        return body;
    }


    public String getCreated_at() {
        return created_at;
    }


    public User getUser() {
        return user;
    }


    public String getHtml_url() {
        return html_url;
    }


    public String getState() {
        return state;
    }



    //métodos Parcelable


    @Override
    public int describeContents() {
        return 0;
    }


    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.title);
        dest.writeString(this.body);
        dest.writeString(this.html_url);
        dest.writeString(this.created_at);
        dest.writeString(this.state);
        dest.writeParcelable(this.user, flags);
        dest.writeByte(this.carregando ? (byte) 1 : (byte) 0);
        dest.writeByte(this.fim ? (byte) 1 : (byte) 0);
    }


    public PR() {
    }


    protected PR(Parcel in) {
        this.title = in.readString();
        this.body = in.readString();
        this.html_url = in.readString();
        this.created_at = in.readString();
        this.state = in.readString();
        this.user = in.readParcelable(User.class.getClassLoader());
        this.carregando = in.readByte() != 0;
        this.fim = in.readByte() != 0;
    }


    public static final Parcelable.Creator<PR> CREATOR = new Parcelable.Creator<PR>() {
        @Override
        public PR createFromParcel(Parcel source) {
            return new PR(source);
        }


        @Override
        public PR[] newArray(int size) {
            return new PR[size];
        }
    };
}
