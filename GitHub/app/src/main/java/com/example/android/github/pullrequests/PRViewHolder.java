package com.example.android.github.pullrequests;

import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.RequestManager;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.example.android.github.R;
import com.example.android.github.pojos.PR;

import java.util.ArrayList;


public class PRViewHolder extends RecyclerView.ViewHolder {

    public TextView titulo, data, body, username, starsNumber, forksNumber;
    public ImageView avatar, state;
    public View linha_bottom;
    public ConstraintLayout layout;


    public PRViewHolder(View itemView) {
        super(itemView);
        layout = itemView.findViewById(R.id.pr_layout);
        titulo = itemView.findViewById(R.id.title_pr);
        data = itemView.findViewById(R.id.data_pr);
        state = itemView.findViewById(R.id.state_iv);
        body = itemView.findViewById(R.id.body_pr);
        username = itemView.findViewById(R.id.username_pr);
        avatar = itemView.findViewById(R.id.avatar_pr);
        linha_bottom = itemView.findViewById(R.id.linha_bottom_pr);

    }


    void bind(Context context, ArrayList<PR> prs, int position, RequestManager glide, final pullRequestsContract.presenter presenter) {

        final PR pr = prs.get(position);

        titulo.setText(pr.getTitle());
        if (pr.getBody() == null || pr.getBody().isEmpty()) {//alguns bodies vem null ao invés de "".
            body.setText(R.string.NaoHaBody);
            body.setTextColor(ContextCompat.getColor(context, R.color.cinza));
            body.setTypeface(null, Typeface.ITALIC);
        } else {
            body.setText(pr.getBody());
            body.setTextColor(ContextCompat.getColor(context, R.color.preto));
            body.setTypeface(null, Typeface.NORMAL);
        }
        username.setText(pr.getUser().getLogin());
        if (pr.getState().equals("open")) {
            state.setImageResource(R.drawable.ic_open);
        } else {
            state.setImageResource(R.drawable.ic_closed);
        }

        String data_string = pr.getCreated_at();
        data_string = data_string.substring(0, data_string.length() - 1);
        data_string = data_string.replaceAll(":", "-");
        data_string = data_string.replaceAll("T", "-");
        String[] s = data_string.split("-");
        int ano = Integer.parseInt(s[0]);
        int mes = Integer.parseInt(s[1]);
        int dia = Integer.parseInt(s[2]);
        int hora = Integer.parseInt(s[3]);
        int minuto = Integer.parseInt(s[4]);
        int segundo = Integer.parseInt(s[5]);
        String data_formatada = dia + "/" + mes + "/" + ano;
        data.setText(data_formatada);

        Drawable placeholder_drawable = ContextCompat.getDrawable(itemView.getContext(), R.drawable.ic_carregando);
        ((Animatable)placeholder_drawable).stop();
        ((Animatable)placeholder_drawable).start();

        glide.load(pr.getUser().getAvatar_url())
                .placeholder(placeholder_drawable)
                .error(R.drawable.ic_erro)
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        Log.d("meuGLIDE", "Exception:" + e.toString());
                        return false;
                    }


                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
//                            Log.d("meuGLIDE", "ResourceReady.");
                        return false;
                    }
                })
                .into(avatar);

        //linha divisória não deve aparecer após o último item.
        if (position == (prs.size() - 1)) {
            linha_bottom.setVisibility(View.INVISIBLE);
        } else {
            linha_bottom.setVisibility(View.VISIBLE);
        }

        layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.openPRinBrowser(pr.getHtml_url());
            }
        });
    }
}
