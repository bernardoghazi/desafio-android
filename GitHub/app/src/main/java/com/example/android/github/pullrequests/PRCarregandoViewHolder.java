package com.example.android.github.pullrequests;

import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.example.android.github.R;


public class PRCarregandoViewHolder extends RecyclerView.ViewHolder {


    public PRCarregandoViewHolder(View itemView) {
        super(itemView);
        ImageView iv = itemView.findViewById(R.id.carregando_iv);
        Drawable drawable = iv.getDrawable();
        ((Animatable) drawable).stop();
        ((Animatable) drawable).start();
    }
}
