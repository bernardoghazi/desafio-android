package com.example.android.github.pullrequests;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import com.example.android.github.GitHubClient;
import com.example.android.github.pojos.PR;

import java.util.ArrayList;

public class PRPresenter implements pullRequestsContract.presenter{

    private Context context;
    private pullRequestsContract.view view;
    private pullRequestsContract.model model;


    PRPresenter(Context context, pullRequestsContract.view view) {
        this.context = context;
        this.view = view;
        model = new PRModel(this);
    }


    @Override
    public void getPRs(GitHubClient client, String criador, String repositorio, int pag) {
            model.loadPRs(client, criador, repositorio, pag);
    }


    @Override
    public void openPRinBrowser(String url) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(url));
        context.startActivity(intent);
    }


    @Override
    public void receberPRs(ArrayList<PR> listPR) {
        view.receberPRs(listPR);
    }
}
