package com.example.android.github.pojos;

import android.os.Parcel;
import android.os.Parcelable;

public class Repo implements Parcelable {
    private String name;
    private String description;
    private int forks_count;
    private int stargazers_count;
    private User owner;
    private boolean carregando;
    private boolean fim;


    public String getName() {
        return name;
    }


    public String getDescription() {
        return description;
    }


    public int getForks_count() {
        return forks_count;
    }


    public int getStargazers_count() {
        return stargazers_count;
    }


    public User getOwner() {
        return owner;
    }


    public boolean isCarregando() {
        return carregando;
    }


    public void setCarregando(boolean carregando) {
        this.carregando = carregando;
    }


    public boolean isFim() {
        return fim;
    }


    public void setFim(boolean fim) {
        this.fim = fim;
    }



    //métodos Parcelable

    @Override
    public int describeContents() {
        return 0;
    }


    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeString(this.description);
        dest.writeInt(this.forks_count);
        dest.writeInt(this.stargazers_count);
        dest.writeParcelable(this.owner, flags);
        dest.writeByte(this.carregando ? (byte) 1 : (byte) 0);
        dest.writeByte(this.fim ? (byte) 1 : (byte) 0);
    }


    public Repo() {
    }


    protected Repo(Parcel in) {
        this.name = in.readString();
        this.description = in.readString();
        this.forks_count = in.readInt();
        this.stargazers_count = in.readInt();
        this.owner = in.readParcelable(User.class.getClassLoader());
        this.carregando = in.readByte() != 0;
        this.fim = in.readByte() != 0;
    }


    public static final Parcelable.Creator<Repo> CREATOR = new Parcelable.Creator<Repo>() {
        @Override
        public Repo createFromParcel(Parcel source) {
            return new Repo(source);
        }


        @Override
        public Repo[] newArray(int size) {
            return new Repo[size];
        }
    };
}