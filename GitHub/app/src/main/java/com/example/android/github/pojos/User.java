package com.example.android.github.pojos;

import android.os.Parcel;
import android.os.Parcelable;

public class User implements Parcelable {
    String login;
    String avatar_url;


    public String getLogin() {
        return login;
    }


    public String getAvatar_url() {
        return avatar_url;
    }




    //métodos Parcelable



    @Override
    public int describeContents() {
        return 0;
    }


    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.login);
        dest.writeString(this.avatar_url);
    }


    public User() {
    }


    protected User(Parcel in) {
        this.login = in.readString();
        this.avatar_url = in.readString();
    }


    public static final Parcelable.Creator<User> CREATOR = new Parcelable.Creator<User>() {
        @Override
        public User createFromParcel(Parcel source) {
            return new User(source);
        }


        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };
}
