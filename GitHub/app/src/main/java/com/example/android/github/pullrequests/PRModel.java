package com.example.android.github.pullrequests;

import android.util.Log;

import com.example.android.github.GitHubClient;
import com.example.android.github.pojos.PR;

import java.io.IOException;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PRModel implements pullRequestsContract.model{

    private pullRequestsContract.presenter presenter;


    PRModel(pullRequestsContract.presenter presenter) {
        this.presenter = presenter;
    }


    @Override
    public void loadPRs(GitHubClient client, String criador, String repositorio, int pag) {
        Call<ArrayList<PR>> callPR = client.pullRequest(criador, repositorio, pag);
        Log.d("RESPOSTA_PR", callPR.request().url().toString());

        callPR.enqueue(new Callback<ArrayList<PR>>() {
            @Override
            public void onResponse(Call<ArrayList<PR>> call, Response<ArrayList<PR>> response) {
//                Log.d("RESPOSTA_JAVA_PRs", "foi");
                presenter.receberPRs(response.body());

                if (response.raw().cacheResponse() != null && response.raw().networkResponse() != null) {
                    // true: response was served from cache
                    Log.d("RESPOSTA_JAVA_PRs", "conteúdo veio do CACHE, header veio do SERVIDOR ('304 - not modified')");
                    Log.d("RESPOSTA_JAVA_REPOS", response.raw().toString());
                } else if (response.raw().cacheResponse() != null) {
                    // true: response was served from cache
                    Log.d("RESPOSTA_JAVA_PRs", "veio do CACHE");
                    Log.d("RESPOSTA_JAVA_PRs", response.raw().cacheResponse().toString());
                } else if (response.raw().networkResponse() != null) {
                    // true: response was served from network/server
                    Log.d("RESPOSTA_JAVA_PRs", "veio do SERVIDOR");
                    Log.d("RESPOSTA_JAVA_PRs", response.raw().networkResponse().toString());
                }
            }


            @Override
            public void onFailure(Call<ArrayList<PR>> call, Throwable t) {
                Log.d("RESPOSTA", "falhou");
                if (t instanceof IOException) {
                    Log.d("RESPOSTA_JAVA_PRs", "erro de conexão -> IOException");
                } else {
                    Log.d("RESPOSTA_JAVA_PRs", "algum outro erro");
                }
            }
        });
    }
}
