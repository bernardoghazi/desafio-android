package com.example.android.github.pullrequests;

import com.example.android.github.GitHubClient;
import com.example.android.github.pojos.PR;

import java.util.ArrayList;

public interface pullRequestsContract {
    interface view {
        void receberPRs(ArrayList<PR> ListPR);
    }

    interface presenter {
        void getPRs(GitHubClient client, String criador, String repositorio, int pag);
        void openPRinBrowser(String url);
        void receberPRs(ArrayList<PR> listPR);
    }

    interface model {
        void loadPRs(GitHubClient client, String criador, String repositorio, int pag);
    }
}
